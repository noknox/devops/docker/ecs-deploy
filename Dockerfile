FROM golang:alpine3.11

RUN apk add autoconf automake build-base curl git libtool jq python3-dev && \
    pip3 install --no-cache --upgrade awscli && \
    go get -v -u github.com/Autodesk/go-awsecs/cmd/update-aws-ecs-service

ENTRYPOINT [ "update-aws-ecs-service" ]